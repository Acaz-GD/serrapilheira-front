import { useState } from 'react';
import { Collapse, Divider, Fab, IconButton } from '@mui/material';
import SearchFilter from './SearchFilter';
import TerritoryGroupFilter from './TerritoryGroupFilter';
import { ReactComponent as LeftIcon } from '../filter/arrowLeftIcon.svg'; // ícone seta esquerda
import { ReactComponent as RightIcon } from '../filter/arrowRightIcon.svg'; // ícone seta direita
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';

const LeftToRightPanel: React.FC = () => {
  const [isOpen, setIsOpen] = useState(true); // Painel aberto por padrão
  const [isArrowLeft, setIsArrowLeft] = useState(true); // Define se o ícone do botão é ArrowLeft ou ArrowRight
  const [filters, setFilters] = useState({ search: '', territoryGroup: '' }); // Estado para armazenar os filtros

  const togglePanel = () => {
    setIsOpen(!isOpen);
    setIsArrowLeft(!isArrowLeft);
  };

  const clearFilters = () => {
    setFilters({ search: '', territoryGroup: '' }); // Limpa os filtros
  };

  return (
    <div className='h-12'>
      {/* Botão para abrir/fechar o painel */}
      <Fab
        onClick={togglePanel}
        sx={{
          marginLeft: '14px',
          height: '36px',
          width: '36px',
          borderRadius: '6px',
          top: '65px',
          padding: '0px',
          background: '#FFF',
          border: '2px solid #BCCAD8',
          boxShadow: 'none', // Remove o drop shadow
          cursor: 'pointer',
          '&:hover': {
            background: '#E0E7FF', // Cor de fundo ao passar o mouse
          },
        }}
      >
        {isArrowLeft ? <LeftIcon /> : <RightIcon />}
      </Fab>

      {/* Painel */}
      <Collapse
        in={isOpen}
        className="absolute z-0"
        style={{
          top: '48px',
          height: '70px',
          display: 'flex',
          background: '#FFF',
          borderRadius: '0px 0px 12px 0px',
          zIndex: '10',
        }}
        orientation="horizontal"
      >
        {/* Conteúdo do painel */}
        <div className="flex items-center justify-center my-4 pl-16 pr-2">
          <Divider
            orientation="vertical"
            variant="middle"
            flexItem
            style={{ margin: '5px 10px', height: '30px' }}
          />
          <TerritoryGroupFilter value={filters.territoryGroup} onChange={(value) => setFilters((prev) => ({ ...prev, territoryGroup: value || '' }))} />
          <SearchFilter value={filters.search} onChange={(value) => setFilters((prev) => ({ ...prev, search: value || '' }))} />

          {/* Botão LIMPAR FILTROS */}
          <IconButton
            onClick={clearFilters}
            sx={{
              marginRight: '14px',
              width: '36px',
              height: '36px',
              borderRadius: '6px',
              padding: '0px',
              background: '#FFF',
              border: '2px solid #BCCAD8',
              boxShadow: 'none', // Remove o drop shadow
              cursor: 'pointer',
              '&:hover': {
                background: '#F5D5D6', // Cor de fundo ao passar o mouse
              },
            }}
          >
             <DeleteOutlineIcon sx={{ color: '#F94939' }} />
          </IconButton>
        </div>
      </Collapse>
    </div>
  );
};

export default LeftToRightPanel;
