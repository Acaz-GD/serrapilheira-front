import * as React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import axios from 'axios';

interface TerritoryGroupFilterProps {
  value: string | null;
  onChange: (value: string | null) => void;
}

const TerritoryGroupFilter: React.FC<TerritoryGroupFilterProps> = ({ value, onChange }) => {
  const [inputValue, setInputValue] = React.useState('');
  const [options, setOptions] = React.useState<string[]>([]);

  React.useEffect(() => {
    const fetchTerritoryGroups = async () => {
      try {
        const response = await axios.get('https://servicodados.ibge.gov.br/api/v1/localidades/estados');
        const territoryGroupsData = response.data.map((state: any) => state.nome);
        setOptions(territoryGroupsData);
      } catch (error) {
        console.error('Erro ao buscar os grupos territoriais:', error);
      }
    };

    fetchTerritoryGroups();
  }, []);

  return (
    <Autocomplete
      size="small"
      value={value}
      onChange={(event: any, newValue: string | null) => {
        onChange(newValue);
      }}
      inputValue={inputValue}
      onInputChange={(event, newInputValue) => {
        setInputValue(newInputValue);
      }}
      id="agrupamentoTerritorial"
      options={options}
      style={{ 
        width: '300px',
        padding: '0px 12px'
      }}
      renderInput={(params) => <TextField {...params} label="Agrupamento Territorial" placeholder="Selecione a opção" />}
    />
  );
};

export default TerritoryGroupFilter;
