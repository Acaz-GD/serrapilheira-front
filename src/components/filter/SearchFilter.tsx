import * as React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import axios from 'axios';

interface SearchFilterProps {
  value: string | null;
  onChange: (value: string | null) => void;
}

const SearchFilter: React.FC<SearchFilterProps> = ({ value, onChange }) => {
  const [inputValue, setInputValue] = React.useState('');
  const [options, setOptions] = React.useState<string[]>([]);

  React.useEffect(() => {
    const fetchStates = async () => {
      try {
        const response = await axios.get('https://servicodados.ibge.gov.br/api/v1/localidades/estados');
        const statesData = response.data.map((state: any) => state.nome);
        setOptions(statesData);
      } catch (error) {
        console.error('Erro ao buscar os estados:', error);
      }
    };

    fetchStates();
  }, []);

  return (
    <Autocomplete
      size="small"
      value={value}
      onChange={(event: any, newValue: string | null) => {
        onChange(newValue);
      }}
      inputValue={inputValue}
      onInputChange={(event, newInputValue) => {
        setInputValue(newInputValue);
      }}
      id="BuscarFiltro"
      options={options}
      sx={{ width: 300, padding: '0px 12px' }}
      renderInput={(params) => <TextField {...params} label="Buscar" placeholder="Selecione a opção" />}
    />
  );
};

export default SearchFilter;
