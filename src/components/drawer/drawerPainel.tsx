import React, { useState } from 'react';
import { Box, Button, Drawer, Fab } from '@mui/material';
import { ChevronLeft, ChevronRight } from '@mui/icons-material';
import TabPainel from './panel/tabPainel';

const FloatingPanelButton: React.FC = () => {
  const [isPanelOpen, setIsPanelOpen] = useState(false);

  const togglePanel = () => {
    setIsPanelOpen(!isPanelOpen);
  };

  return (
    <div className="absolute top-16 right-4 z-50">
      {/* Abrir Painel lateral direito */}
      <Button
        onClick={togglePanel}
        variant="contained"
        sx={{
          backgroundColor: '#002FA7', // Cor de fundo do botão
          color: '#fff', // Cor do texto do botão
          '&:hover': {
            backgroundColor: '#1565C0', // Cor de fundo ao passar o mouse
          },
        }}
      >
        {isPanelOpen ? <ChevronRight /> : <ChevronLeft />}
        Estatística
      </Button>

      <Drawer
        anchor="right"
        open={isPanelOpen}
        variant="persistent"
        sx={{
          paper: 'w-100',
          background: '#fff',
          
        }}
      >
        <Box
          className="drawer-scrollbar"
          sx={{
            width: '32.5rem',
            background: '#fff',
            padding: '12px',
            overflowY: 'auto',
            marginBottom: '4rem',
          }}
        >
          {/* tab de navegação do Painel */}
          <TabPainel />

          {/* Botão de Fechar Painel */}
          <Fab
            onClick={togglePanel}
            sx={{
              position: 'absolute',
              height: '40px',
              borderRadius: '6px',
              width: '40px',
              top: '18px',
              right: '23px',
              padding: '0px',
              background: '#fff',
              boxShadow: 'none', // Remove o drop shadow
              cursor: 'pointer',
              '&:hover': {
                background: '#E0E7FF', // Cor de fundo ao passar o mouse
              },
            }}
          >
            <ChevronRight />
          </Fab>
        </Box>
      </Drawer>
    </div>
  );
};

export default FloatingPanelButton;
