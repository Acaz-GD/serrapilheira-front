import * as React from 'react';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Link from '@mui/material/Link';
import MovingIcon from '@mui/icons-material/Moving';
import BarChartIcon from '@mui/icons-material/BarChart';
import Indicadores from './breadcrumbs/indicadores';
import Estatisticas from './breadcrumbs/estatisticas';

function IconBreadcrumbs() {
  const indicadoresRef = React.useRef<HTMLDivElement>(null);
  const estatisticasRef = React.useRef<HTMLDivElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>, section: string) => {
    event.preventDefault();
    if (section === 'indicadores' && indicadoresRef.current) {
      indicadoresRef.current.scrollIntoView({ behavior: 'smooth' });
    } else if (section === 'estatisticas' && estatisticasRef.current) {
      estatisticasRef.current.scrollIntoView({ behavior: 'smooth' });
    }
    console.info('Você clicou na rota do breadcrumb:', section);
  };

  return (
    <div role="presentation">
      {/* título header Visão Geral */}
      <h2 className="text-lg font-bold text-gray-500 mt-2">Visão Geral</h2>
      <h3 className="text-xs font-normal italic text-blue-500 mb-6">
        Informações rápidas, ex: pequeno parágrafo para o tipo de Perfil.
      </h3>

      {/* Rotas de navegação rápida */}
      <Breadcrumbs aria-label="breadcrumb">
        <Link
          underline="hover"
          sx={{ display: 'flex', alignItems: 'center' }}
          color="inherit"
          href="#"
          onClick={(e) => handleClick(e, 'indicadores')}
        >
          <MovingIcon sx={{ mr: 0.5 }} fontSize="inherit" />
          Indicadores
        </Link>

        <Link
          underline="hover"
          sx={{ display: 'flex', alignItems: 'center' }}
          color="inherit"
          href="#"
          onClick={(e) => handleClick(e, 'estatisticas')}
        >
          <BarChartIcon sx={{ mr: 0.5 }} fontSize="inherit" />
          Estatísticas
        </Link>
      </Breadcrumbs>

      {/* Breadcrumbs: indicadores/estatísticas */}
      <div ref={indicadoresRef}>
        <Indicadores />
      </div>
      <div ref={estatisticasRef}>
        <Estatisticas />
      </div>
    </div>
  );
}

export default IconBreadcrumbs;
