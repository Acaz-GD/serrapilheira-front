import React from 'react';
import { Container, Divider, Typography } from '@mui/material';
import { ReactComponent as AgriculturaIcon } from '../../../../../assets-components/agriculturaIcon.svg';
import { ReactComponent as PastagemIcon } from '../../../../../assets-components/pastagemIcon.svg';
import { ReactComponent as FlorestaIcon } from '../../../../../assets-components/florestaIcon.svg';
import { ReactComponent as AntropicosIcon } from '../../../../../assets-components/antropicosIcon.svg';
import { ReactComponent as MineracaoIcon } from '../../../../../assets-components/mineracaoIcon.svg';
import { IconButton, Tooltip } from '@mui/material';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';

const Indicadores = React.forwardRef<HTMLDivElement>((props, ref) => {
  return (
    <Container ref={ref}>
      <Divider style={{ margin: '12px 0px' }} />

      {/* Título principal */}
      <div style={{ display: 'flex', alignItems: 'center' }}>
      <Typography sx={{ fontSize: '16px', fontWeight: 600, color: '#374151', marginTop: '5px' }}> Indicadores </Typography>
        <Tooltip title="Descrição sobre os indicadores">
          <IconButton size="small" style={{ marginLeft: 8, marginTop: 8, color: '#A3AED0' }}>
            <InfoOutlinedIcon fontSize="small" />
          </IconButton>
        </Tooltip>
      </div>

      {/* Painel de Indicadores */}
      <div className="flex flex-wrap mt-4">
        {/* Agricultura */}
        <div className="flex items-start w-1/2 p-2">
          <AgriculturaIcon className="mr-2 w-8" />
          <div>
            <Typography sx={{ fontSize: '13px', fontWeight: 600, color: '#495057'}}>Agricultura</Typography>
            <Typography sx={{ fontSize: '13px', color: '#495057'}}>área selecionada</Typography>
          </div>
        </div>
        {/* Pastagem */}
        <div className="flex items-start w-1/2 p-2">
          <PastagemIcon className="mr-2 w-8" />
          <div>
            <Typography sx={{ fontSize: '13px', fontWeight: 600, color: '#495057'}}>Pastagem</Typography>
            <Typography sx={{ fontSize: '13px', color: '#495057'}}>área selecionada </Typography>
          </div>
        </div>
        {/* Formação de Floresta */}
        <div className="flex items-start w-1/2 p-2">
          <FlorestaIcon className="mr-4" />
          <div>
            <Typography sx={{ fontSize: '13px', fontWeight: 600, color: '#495057'}}>Formação de Floresta</Typography>
            <Typography sx={{ fontSize: '13px', color: '#495057'}}>área selecionada</Typography>
          </div>
        </div>
        {/* Formação Natural */}
        <div className="flex items-start w-1/2 p-2">
          <AntropicosIcon className="mr-4" />
          <div>
            <Typography sx={{ fontSize: '13px', fontWeight: 600, color: '#495057'}}>Formação Natural</Typography>
            <Typography sx={{ fontSize: '13px', color: '#495057'}}>área selecionada</Typography>
          </div>
        </div>
        {/* Formação não florestal*/}
        <div className="flex items-start w-1/2 p-2">
          <MineracaoIcon className="mr-4" />
          <div>
            <Typography sx={{ fontSize: '13px', fontWeight: 600, color: '#495057'}}>Formação não floresta</Typography>
            <Typography sx={{ fontSize: '13px', color: '#495057'}}>área selecionada</Typography>
          </div>
        </div>
        {/* Usos Antrópicos*/}
        <div className="flex items-start w-1/2 p-2">
          <AgriculturaIcon className="mr-4" />
          <div>
            <Typography sx={{ fontSize: '13px', fontWeight: 600, color: '#495057'}}>Usos Antrópicos</Typography>
            <Typography sx={{ fontSize: '13px', color: '#495057'}}>área selecionada</Typography>
          </div>
        </div>
      </div>
    </Container>
  );
});

export default Indicadores;
