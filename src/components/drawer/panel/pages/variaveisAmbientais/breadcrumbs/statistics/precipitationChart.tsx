// src/components/PrecipitationChart.tsx
import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

const options: Highcharts.Options = {
  chart: {
    type: 'spline',  // Defina um tipo padrão de gráfico
    zooming: {
      type: 'xy'
    }
  },
  title: {
    text: 'Precipitação e Temperatura'
  },
  xAxis: [{
    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    crosshair: true
  }],
  yAxis: [{ // Primary yAxis
    labels: {
      format: '{value}°C',
      style: {
        color: Highcharts.getOptions().colors?.[1] as string
      }
    },
    title: {
      text: 'Temperatura',
      style: {
        color: Highcharts.getOptions().colors?.[1] as string
      }
    }
  }, { // Secondary yAxis
    title: {
      text: 'Precipitação',
      style: {
        color: Highcharts.getOptions().colors?.[0] as string
      }
    },
    labels: {
      format: '{value} mm',
      style: {
        color: Highcharts.getOptions().colors?.[0] as string
      }
    },
    opposite: true
  }],
  tooltip: {
    shared: true
  },
  legend: {
    layout: 'vertical',
    align: 'left',
    x: 80,
    verticalAlign: 'top',
    y: 55,
    floating: true,
    backgroundColor: Highcharts.defaultOptions.legend?.backgroundColor || 'rgba(255,255,255,0.25)'
  },
  series: [{
    name: 'Precipitação',
    type: 'column',
    yAxis: 1,
    data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
    tooltip: {
      valueSuffix: ' mm'
    }
  }, {
    name: 'Temperatura',
    type: 'spline',
    data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
    tooltip: {
      valueSuffix: '°C'
    }
  }]
};

const PrecipitationChart: React.FC = () => {
  return (
    <div className="p-4 shadow-lg bg-white">
      <HighchartsReact
        highcharts={Highcharts}
        options={options}
      />
    </div>
  );
};

export default PrecipitationChart;
