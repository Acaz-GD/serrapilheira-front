import React, { useState } from 'react';
import { Tooltip, IconButton, Select, MenuItem, Checkbox, FormControl, InputLabel, FormControlLabel, Box, Divider } from '@mui/material';
import ChecklistIcon from '@mui/icons-material/Checklist';
import { SelectChangeEvent } from '@mui/material/Select';
import PrecipitationChart from './statistics/precipitationChart'; // grafico 1
import TemperatureChart from './statistics/temperatureChart'; // grafico 2
import HistogramChart from './statistics/histogramChart'; // grafico 3
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';

const ClimateVariables: React.FC = () => {
  const [panelOpen, setPanelOpen] = useState(false);
  const [variable1, setVariable1] = useState<string>('');
  const [variable2, setVariable2] = useState<string>('');
  const [checkbox1, setCheckbox1] = useState<boolean>(false);
  const [checkbox2, setCheckbox2] = useState<boolean>(false);
  const [checkbox3, setCheckbox3] = useState<boolean>(false);
  const [checkbox4, setCheckbox4] = useState<boolean>(false);

  const handlePanelToggle = () => {
    setPanelOpen(!panelOpen);
  };

  const handleVariable1Change = (event: SelectChangeEvent<string>) => {
    setVariable1(event.target.value);
  };

  const handleVariable2Change = (event: SelectChangeEvent<string>) => {
    setVariable2(event.target.value);
  };

  const handleCheckboxChange = (checkboxSetter: React.Dispatch<React.SetStateAction<boolean>>) => () => {
    checkboxSetter(prev => !prev);
  };

  return (
    <div>
      <Divider style={{ margin: '12px 0px' }} />

      <div style={{ display: 'flex', alignItems: 'center' }}>
      <h2 className="text-sm font-bold text-gray-700 mt-2">Variáveis Climáticas</h2>
      <Tooltip title="Descrição sobre as Variáveis Climáticas">
          <IconButton size="small" style={{ marginLeft: 8, marginTop: 6, color: '#A3AED0' }}>
            <InfoOutlinedIcon fontSize="small" />
          </IconButton>
        </Tooltip>
      </div>
      <h3 className="text-xs font-normal italic text-blue-500 mb-6">Estatísticas de Variáveis Climáticas</h3>

      <div className="flex items-center space-x-2">
        {/* Filtro Modelo climatico */}
        <FormControl variant="outlined" size="small" className="w-56">
          <InputLabel className='text-xs'>Modelo Climático</InputLabel>
          <Select className='text-xs'
            value={variable1}
            onChange={handleVariable1Change}
            label="Variable 1">
            <MenuItem value="CESM2">CESM2</MenuItem>
            <MenuItem value="CMCC-ESM2">CMCC-ESM2</MenuItem>
            <MenuItem value="GFDL">GFDL</MenuItem>
            <MenuItem value="TaiESM1">TaiESM1</MenuItem>
          </Select>
        </FormControl>

        {/* Filtro Cenario */}
        <FormControl variant="outlined" size="small" className="w-56">
          <InputLabel>Cenário</InputLabel>
          <Select className='text-xs'
            value={variable2}
            onChange={handleVariable2Change}
            label="Variable 2" >
            <MenuItem value="ssp245">ssp245</MenuItem>
            <MenuItem value="ssp585">ssp585</MenuItem>
          </Select>
        </FormControl>

        {/* Filtros Estatisticas  */}
        <IconButton onClick={handlePanelToggle}>
          <ChecklistIcon />
        </IconButton>
      </div>

      {panelOpen && (
        <Box
          className="mt-4"
          sx={{
            position: 'absolute',
            top:'360px',
            right: '100px',
            width: '20rem',
            height: '13rem',
            border: '1px solid #ccc',
            borderRadius: '8px',
            padding: '16px',
            display: 'flex',
            flexDirection: 'column',
            gap: '2px',
            backgroundColor: 'white',
            boxShadow: 3,
            zIndex: 1,
          }}
        >
          <div
            role="button"
            onClick={handleCheckboxChange(setCheckbox1)}
            onKeyPress={handleCheckboxChange(setCheckbox1)}
            tabIndex={0}
            style={{ display: 'flex', alignItems: 'center', padding: '0px 8px', borderRadius: '4px', cursor: 'pointer', backgroundColor: checkbox1 ? '#f0f0f0' : 'transparent' }}
            className="hover:bg-gray-200"
          >
            <Checkbox checked={checkbox1} onChange={handleCheckboxChange(setCheckbox1)} />
            <FormControlLabel
              control={<span />}
              label="Camada de Municípios"
              sx={{ margin: 0 }}
            />
          </div>
          <div
            role="button"
            onClick={handleCheckboxChange(setCheckbox2)}
            onKeyPress={handleCheckboxChange(setCheckbox2)}
            tabIndex={0}
            style={{ display: 'flex', alignItems: 'center', padding: '0px 8px', borderRadius: '4px', cursor: 'pointer', backgroundColor: checkbox2 ? '#f0f0f0' : 'transparent' }}
            className="hover:bg-gray-200"
          >
            <Checkbox checked={checkbox2} onChange={handleCheckboxChange(setCheckbox2)} />
            <FormControlLabel
              control={<span />}
              label="Gráfico de Temperatura do Ar"
              sx={{ margin: 0 }}
            />
          </div>
          <div
            role="button"
            onClick={handleCheckboxChange(setCheckbox3)}
            onKeyPress={handleCheckboxChange(setCheckbox3)}
            tabIndex={0}
            style={{ display: 'flex', alignItems: 'center', padding: '0px 8px', borderRadius: '4px', cursor: 'pointer', backgroundColor: checkbox3 ? '#f0f0f0' : 'transparent' }}
            className="hover:bg-gray-200"
          >
            <Checkbox checked={checkbox3} onChange={handleCheckboxChange(setCheckbox3)} />
            <FormControlLabel
              control={<span />}
              label="Gráfico de Precipitação"
              sx={{ margin: 0 }}
            />
          </div>
          <div
            role="button"
            onClick={handleCheckboxChange(setCheckbox4)}
            onKeyPress={handleCheckboxChange(setCheckbox4)}
            tabIndex={0}
            style={{ display: 'flex', alignItems: 'center', padding: '0px 8px', borderRadius: '4px', cursor: 'pointer', backgroundColor: checkbox4 ? '#f0f0f0' : 'transparent' }}
            className="hover:bg-gray-200"
          >
            <Checkbox checked={checkbox4} onChange={handleCheckboxChange(setCheckbox4)} />
            <FormControlLabel
              control={<span />}
              label="Histograma de Npp"
              sx={{ margin: 0 }}
            />
          </div>
        </Box>
      )}

       {/* Abrir graficos  */}

      {checkbox2 && (
        <Box sx={{ marginTop: '16px' }}>
          <TemperatureChart />
        </Box>
      )}

      {checkbox3 && (
        <Box sx={{ marginTop: '16px' }}>
          <PrecipitationChart />
        </Box>
      )}

      {checkbox4 && (
        <Box sx={{ marginTop: '16px' }}>
          <HistogramChart />
        </Box>
      )}
    </div>
  );
};

export default ClimateVariables;
