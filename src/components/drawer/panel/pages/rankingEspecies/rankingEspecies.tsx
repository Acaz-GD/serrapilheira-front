import * as React from 'react';
import { DataGrid, GridFilterModel, GridToolbar } from '@mui/x-data-grid';
import { Tooltip, IconButton } from '@mui/material';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';

interface RowData {
  id: number;
  species: string;
  climateResistance: string;
  uses: string;
  domesticated: string;
  seedCost: string;
}

const columns = [
  { field: 'id', headerName: '#', width: 10 },
  { field: 'species', headerName: 'Espécies', width: 100 },
  { field: 'climateResistance', headerName: 'Resistência às mudanças do clima', width: 100 },
  { field: 'uses', headerName: 'Usos', width: 60 },
  { field: 'domesticated', headerName: 'Domesticada', width: 80 },
  { field: 'seedCost', headerName: 'Custo da Semente', width: 80 },
];

const rows: RowData[] = Array.from({ length: 30 }, (_, id) => ({
  id: id + 1,
  species: `Espécie ${id + 1}`,
  climateResistance: `Resistência ${id + 1}`,
  uses: `Uso ${id + 1}`,
  domesticated: id % 2 === 0 ? 'Sim' : 'Não',
  seedCost: `$${(Math.random() * 100).toFixed(2)}`,
}));

export default function ControlledFilters() {
  const [filterModel, setFilterModel] = React.useState<GridFilterModel>({
    items: [],
  });

  return (
    <div style={{ height: '37.9rem', width: '100%', padding: '0px' }}>
      {/* Titulo header Ranking de Especies */}
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <h2 className="text-lg font-bold text-gray-500 mt-2">Prioridades da Área</h2>
        <Tooltip title="Descrição sobre áreas prioritárias">
          <IconButton size="small" style={{ marginLeft: 8, marginTop: 8, color: '#A3AED0' }}>
            <InfoOutlinedIcon fontSize="small" />
          </IconButton>
        </Tooltip>
      </div>
      <h3 className="text-xs font-normalS text-blue-500 mb-6">Ranking das 30 principais espécies.</h3>
      
      <DataGrid
        rows={rows}
        columns={columns}
        slots={{
          toolbar: GridToolbar,
        }}
        filterModel={filterModel}
        onFilterModelChange={(newFilterModel) => setFilterModel(newFilterModel)}
        sx={{
          '&.MuiDataGrid-root': {
            padding: 0,
          },
          '& .MuiDataGrid-cell': {
            fontSize: '12px', // Tamanho da fonte das células
          },
          '& .MuiDataGrid-columnHeaderTitle': {
            fontSize: '12px', // Tamanho da fonte dos cabeçalhos
          },
          '& .MuiButtonBase-root': {
            fontSize: '12px', // Tamanho da fonte dos botões
          },
        }}
      />
    </div>
  );
}
