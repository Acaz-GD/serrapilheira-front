import * as React from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import VariaveisAmbientais from './pages/variaveisAmbientais/variaveisAmbientais';
import RankingEspecies from './pages/rankingEspecies/rankingEspecies';

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function CustomTabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  // Tab de navegação entre Variaveis Ambientais e Ranking de Especies
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 1 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function BasicTabs() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: '140'}}>
        
      <Box sx={{ borderBottom: 1, borderColor: 'divider', display:'flex', justifyContent:'flex-start' }}>
        <Tabs value={value} onChange={handleChange} 
        style={{
          marginRight: '20px',
          color:'#002FA7',
        }}>

          <Tab label="Variáveis Ambientais" {...a11yProps(0)} 
          sx={{
              fontSize: '0.75rem',
              fontWeight: 'bold',
          }}/>

          <Tab label="Ranking de Espécies" {...a11yProps(1)} 
              sx={{
                fontSize: '0.75rem',
                fontWeight: 'bold',
            }}/>
          
        </Tabs>
      </Box>
      {/* Tab Variáveis Ambientais */}
      <CustomTabPanel value={value} index={0}>

        <VariaveisAmbientais />

      </CustomTabPanel>

    {/* Tab Ranking de EspÉcies */}
      <CustomTabPanel value={value} index={1}>
        
        <RankingEspecies />
        
      </CustomTabPanel>

    </Box>
  );
}