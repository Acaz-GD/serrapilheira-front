
import * as L from 'leaflet';

// export type AlertValidationMode = 'EDITING' | 'VALIDATING';

export type AlertDataLayer = 'Originais' | 'Sem sobreposição' | 'Refinados';

export type ControlOverLayer = {
    camada: string,
    color?: string,
    fonte?: string,
    ano?: string,
    index?: number,
    downloadUrl?: string,
    isVisible?: boolean,
    layer?: L.Layer,
    getLayer?: () => L.Layer,
    subitens?: ControlOverLayer[],
}

export type ControlBasemap = {
    camada: string,
    color?: string,
    fonte?: string,
    ano?: string,
    downloadUrl?: string,
    isVisible?: boolean,
    layer?: L.Layer,
    getLayer?: () => L.Layer,
    subitens?: ControlBasemap[],
}

export type ControlLegends = {
    camada: string,
    subtitle?: string,
    color?: string,
    fonte?: String,
    ano?: string,
    downloadUrl?: string,
    isVisible?: boolean,
    layer?: L.Layer,
    subitens?: ControlLegends[],
}

export type ControlTerminal = {
    camada: string,
    color?: string,
    fonte?: string,
    ano?: string,
    downloadUrl?: string,
    isVisible?: boolean,
    layer?: L.Layer,
    subitens?: ControlTerminal[],
}

export type ControlPicker = {
    camada: string,
    color?: string,
    fonte?: string,
    ano?: string,
    downloadUrl?: string,
    isVisible?: boolean,
    layer?: L.Layer,
    subitens?: ControlPicker[],
}

export type AlertValidationGeeImage = {
    order: number;
    thumb: string;
    tile: string;
    imgID: string;
    date: Date;
    imgSat: string;
    selected: boolean;
    disabled: boolean;
};

export type GetAlertGeeImagesProcessedParams = {
    alertId: number,
    alertGeometryId: number,
    dateDetection: string,
    polygon: string
}
export type AlertGeeImageProcessed = {
    after: {
        link: string;
        name: string;
        date: string;
        type: string;
        mapid: string;
    };
    before: {
        link: string;
        name: string;
        date: string;
        type: string;
        mapid: string;
    };
    [key: string]: any;
}

export interface UpdateAlertGeeImageProcessed {
    imgBeforeDate?: string;
    imgBefore?:     string;
    imgBeforeUrl?:  string;
    imgBeforeType?: string;
    mapIdBefore?:   string;
    mapIdAfter:    string;
    imgAfterDate:  string;
    imgAfter:      string;
    imgAfterUrl:   string;
    imgAfterType:  string;
    source:        string;
    alertId:       number;
    layerType:     string;
}


/**
 * GeoServer Layer :MAT_VIEW_ALERTS_RAW
 */
export enum GEOSERVER_LAYER_ALERTS_RAW {
    /**
     * type: BigDecimal
     */
    ALERT_ID = 'ALERT_ID',
    /**
     * type: String
     */
    ALERT_SOURCE = 'ALERT_SOURCE',
    /**
     * type: Date
     */
    ALERT_DETECTION_DATE = 'ALERT_DETECTION_DATE',
    /**
     * type: Date
     */
    ALERT_INSERTION_DATE = 'ALERT_INSERTION_DATE',
    /**
     * type: BigDecimal
     */
    ALERT_CHARGEABLE = 'ALERT_CHARGEABLE',
    /**
     * type: Date
     */
    ALERT_DELETED_AT = 'ALERT_DELETED_AT',
    /**
     * type: BigDecimal
     */
    ALERT_DELETED_BY_USER_ID = 'ALERT_DELETED_BY_USER_ID',
    /**
     * type: BigDecimal
     */
    ALERT_GEOMETRY_ID = 'ALERT_GEOMETRY_ID',
    /**
     * type: BigDecimal
     */
    ALERT_GEOMETRY_DETECTION_CLASS_ID = 'ALERT_GEOMETRY_DETECTION_CLASS_ID',
    /**
     * type: BigDecimal
     */
    ALERT_GEOMETRY_AREA = 'ALERT_GEOMETRY_AREA',
    /**
     * type: BigDecimal
     */
    ALERT_GEOMETRY_PROCESSING_LEVEL = 'ALERT_GEOMETRY_PROCESSING_LEVEL',
    /**
     * type: String
     */
    BIOMA_ID = 'BIOMA_ID',
    /**
     * type: String
     */
    ASSENTAMENTO_ID = 'ASSENTAMENTO_ID',
    /**
     * type: String
     */
    MUNICIPIO_ID = 'MUNICIPIO_ID',
    /**
     * type: String
     */
    UC_ID = 'UC_ID',
    /**
     * type: String
     */
    UCS_JURISDICAO = 'UCS_JURISDICAO',
    /**
     * type: String
     */
    ASV_ID = 'ASV_ID',
    /**
     * type: String
     */
    GEOEMBARGO_ID_GEO = 'GEOEMBARGO_ID_GEO',
    /**
     * type: String
     */
    GEO_CAR_SOURCE = 'GEO_CAR_SOURCE',
    /**
     * type: String
     */
    TI_ID = 'TI_ID',
    /**
     * type: String
     */
    REFINED = 'REFINED',
    /**
     * type: String
     */
    EMBARGO_BOOL = 'EMBARGO_BOOL',
    /**
     * type: String
     */
    UC_BOOL = 'UC_BOOL',
    /**
     * type: String
     */
    ASSENTAMENTO_BOOL = 'ASSENTAMENTO_BOOL',
    /**
     * type: String
     */
    ASV_BOOL = 'ASV_BOOL',
    /**
     * type: String
     */
    TI_BOOL = 'TI_BOOL',
}

/**
 * GeoServer Layer :GEOSERVER_ALERT_REFINED
 */
export enum GEOSERVER_LAYER_ALERTS_REFINED {
    /**
     * type: BigDecimal
     */
    ALERT_ID = 'ALERT_ID',
    /**
     * type: String
     */
    ALERT_SOURCE = 'ALERT_SOURCE',
    /**
     * type: Date
     */
    ALERT_DETECTION_DATE = 'ALERT_DETECTION_DATE',
    /**
     * type: Date
     */
    ALERT_INSERTION_DATE = 'ALERT_INSERTION_DATE',
    /**
     * type: BigDecimal
     */
    ALERT_CHARGEABLE = 'ALERT_CHARGEABLE',
    /**
     * type: Date
     */
    ALERT_DELETED_AT = 'ALERT_DELETED_AT',
    /**
     * type: BigDecimal
     */
    ALERT_DELETED_BY_USER_ID = 'ALERT_DELETED_BY_USER_ID',
    /**
     * type: BigDecimal
     */
    ALERT_GEOMETRY_ID = 'ALERT_GEOMETRY_ID',
    /**
     * type: String
     */
    ALERT_GEOMETRY_DETECTION_CLASS_ID = 'ALERT_GEOMETRY_DETECTION_CLASS_ID',
    /**
     * type: BigDecimal
     */
    ALERT_GEOMETRY_AREA = 'ALERT_GEOMETRY_AREA',

    /**
     * type: BigDecimal
     */
    ALERT_WEIGHT = 'ALERT_WEIGHT',
    /**
     * type: String
     */
    BIOMA_ID = 'BIOMA_ID',
    /**
     * type: String
     */
    ASSENTAMENTO_ID = 'ASSENTAMENTO_ID',
    /**
     * type: String
     */
    MUNICIPIO_ID = 'MUNICIPIO_ID',
    /**
     * type: String
     */
    UC_ID = 'UC_ID',
    /**
     * type: String
     */
    UCS_JURISDICAO = 'UCS_JURISDICAO',
    /**
     * type: String
     */
    ASV_ID = 'ASV_ID',
    /**
     * type: BigDecimal
     */
    VALIDATION_BOOL = 'VALIDATION_BOOL',
    /**
     * type: BigDecimal
     */
    STATUS_ID = 'STATUS_ID',
    /**
     * type: String
     */
    GEOEMBARGO_ID_GEO = 'GEOEMBARGO_ID_GEO',
    /**
     * type: String
     */
    GEO_CAR_SOURCE = 'GEO_CAR_SOURCE',
    /**
     * type: String
     */
    TI_ID = 'TI_ID',
    /**
     * type: String
     */
    REFINED = 'REFINED',
    /**
     * type: String
     */
    EMBARGO_BOOL = 'EMBARGO_BOOL',
    /**
     * type: String
     */
    UC_BOOL = 'UC_BOOL',
    /**
     * type: String
     */
    ASSENTAMENTO_BOOL = 'ASSENTAMENTO_BOOL',
    /**
     * type: String
     */
    ASV_BOOL = 'ASV_BOOL',
    /**
     * type: String
     */
    TI_BOOL = 'TI_BOOL',
}

/**
 * GeoServer Layer :MAT_VIEW_ALERTS_PROCESSED
 */
export enum GEOSERVER_LAYER_ALERTS_PROCESSED {
    /**
     * type: BigDecimal
     */
    ALERT_ID = 'ALERT_ID',
    /**
     * type: String
     */
    ALERT_SOURCE = 'ALERT_SOURCE',
    /**
     * type: Date
     */
    ALERT_DETECTION_DATE = 'ALERT_DETECTION_DATE',
    /**
     * type: Date
     */
    ALERT_INSERTION_DATE = 'ALERT_INSERTION_DATE',
    /**
     * type: BigDecimal
     */
    ALERT_CHARGEABLE = 'ALERT_CHARGEABLE',
    /**
     * type: Date
     */
    ALERT_DELETED_AT = 'ALERT_DELETED_AT',
    /**
     * type: BigDecimal
     */
    ALERT_DELETED_BY_USER_ID = 'ALERT_DELETED_BY_USER_ID',
    /**
     * type: BigDecimal
     */
    ALERT_GEOMETRY_ID = 'ALERT_GEOMETRY_ID',
    /**
     * type: BigDecimal
     */
    ALERT_GEOMETRY_DETECTION_CLASS_ID = 'ALERT_GEOMETRY_DETECTION_CLASS_ID',
    /**
     * type: BigDecimal
     */
    ALERT_GEOMETRY_AREA = 'ALERT_GEOMETRY_AREA',
    /**
     * type: BigDecimal
     */
    ALERT_GEOMETRY_PROCESSING_LEVEL = 'ALERT_GEOMETRY_PROCESSING_LEVEL',
    /**
     * type: String
     */
    BIOMA_ID = 'BIOMA_ID',
    /**
     * type: String
     */
    ASSENTAMENTO_ID = 'ASSENTAMENTO_ID',
    /**
     * type: String
     */
    MUNICIPIO_ID = 'MUNICIPIO_ID',
    /**
     * type: String
     */
    UC_ID = 'UC_ID',
    /**
     * type: String
     */
    UCS_JURISDICAO = 'UCS_JURISDICAO',
    /**
     * type: String
     */
    ASV_ID = 'ASV_ID',
    /**
     * type: String
     */
    GEOEMBARGO_ID_GEO = 'GEOEMBARGO_ID_GEO',
    /**
     * type: String
     */
    GEO_CAR_SOURCE = 'GEO_CAR_SOURCE',
    /**
     * type: String
     */
    TI_ID = 'TI_ID',
    /**
     * type: String
     */
    REFINED = 'REFINED',
    /**
     * type: String
     */
    EMBARGO_BOOL = 'EMBARGO_BOOL',
    /**
     * type: String
     */
    UC_BOOL = 'UC_BOOL',
    /**
     * type: String
     */
    ASSENTAMENTO_BOOL = 'ASSENTAMENTO_BOOL',
    /**
     * type: String
     */
    ASV_BOOL = 'ASV_BOOL',
    /**
     * type: String
     */
    TI_BOOL = 'TI_BOOL',

}


export enum TERRITORY_CATEGORIES {
    ASSENTAMENTO = 'Assentamento',
    UCF = 'Unidades de Conservação - Federal',
    UCE = 'Unidades de Conservação - Estadual',
    TI = 'Terra Indígena',
}

export enum VALIDATION_FILTER_TERRITORY_CATEGORY {
    ASSENTAMENTO = 'Assentamento',
    UCF = 'Unidades de Conservação - Federal',
    UCE = 'Unidades de Conservação - Estadual',
    TI = 'Terra Indígena',
}


export enum MODE {
  CAMPAIGN = 'campaign',
  TARGET = 'target'
}





