
export const ALERT_AREA_MAX = '10000000';
export const ALERT_WEIGHT_MAX = '10';

const pastDate = new Date(new Date().setDate(new Date().getDate() - 780));
const todayDate = new Date();

export const DATE_FILTER_INITIAL = {
    dateStart: pastDate,
    dateEnd: todayDate
};


export const GEOSERVER_WORKSPACE = 'SEMAMT2';

export const ALERT_GEOSERVER_LAYERS = {
    "Originais": `GEOSERVER_ALERT_RAW`,
    "Sem sobreposição": `GEOSERVER_ALERT_PROCESSED`,
    "Refinados": `GEOSERVER_ALERT_REFINED`
};

export const DATA_LAYER_ROUTE_PATHS = {
    "Originais": 'originais',
    "Sem sobreposição": 'semsobreposicao',
    "Refinados": 'refinados',
};

export const ALERT_GEOSERVER_LAYER_LABELS = {
    "Originais": 'Originais',
    "Sem sobreposição": 'Sem sobreposição',
    "Refinados": 'Refinados',
};

export const ALERT_VALIDATION_LAYER = ALERT_GEOSERVER_LAYERS['Refinados'];
export const ALERT_VALIDATION_LAYER_LABEL = 'Refinados';
export const ALERT_VALIDATION_SUPERVISION_LAYER = 'GEOSERVER_ALERT_REFINED_SUPERVISION';

