import { ControlOverLayer } from "../types";
import { tileLayer } from 'leaflet';
import {
    GEOSERVER_URL,
    SEMAMT_GEOSERVER_AUTHKEY,
    SEMAMT_GEOSERVER_URL
} from "./environment";
import { GEOSERVER_WORKSPACE } from ".";

export const MOD_VISUALIZATION_OVERLAYERS: ControlOverLayer[] = [
    {
        camada: "Assentamentos",
        fonte: "SEMA",
        ano: "2022",
        downloadUrl: `${GEOSERVER_URL}/${GEOSERVER_WORKSPACE}/wms?service=WMS&version=1.1.0&request=GetMap&layers=SEMAMT%3AGEO_ASSENTAMENTO&bbox=-61.6357039%2C-18.0437625%2C-50.22456922%2C-7.348650141&width=768&height=719&srs=EPSG%3A4326&styles=&format=image%2Ftiff`,
        layer: tileLayer.wms(`${GEOSERVER_URL}/${GEOSERVER_WORKSPACE}/wms`, {
            attribution: 'SEMA',
            layers: `${GEOSERVER_WORKSPACE}:GEO_ASSENTAMENTO`,
            format: 'image/png',
            transparent: true
        }),
    },
    {
        camada: "Autorizações",
        fonte: "SEMA",
        ano: "2022",
        downloadUrl: "https://geo.sema.mt.gov.br/geoserver/web",
        subitens: [
            {
                camada: "AD",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3AAUTORIZACAO_DESMATE_SEMA&outputFormat=SHAPE-ZIP",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:AUTORIZACAO_DESMATE_SEMA',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
            {
                camada: "AEF",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3AAUTORIZACAO_EXPLORACAO_SEMA&outputFormat=SHAPE-ZIP",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:AUTORIZACAO_EXPLORACAO_SEMA',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
            {
                camada: "APF",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3AMVW_APF_GEOMETRIA_REGULAR&outputFormat=SHAPE-ZIP",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:MVW_APF_GEOMETRIA_REGULAR',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
            {
                camada: "AQC",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3AAUTORIZACAO_QUEIMA_CONTROLADA_SEMA&outputFormat=SHAPE-ZIP",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:AUTORIZACAO_QUEIMA_CONTROLADA_SEMA',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
            {
                camada: "AUTEX",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3AAUTORIZACAO_AUTEX_PMFS_SEMA&outputFormat=SHAPE-ZIP",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:AUTORIZACAO_AUTEX_PMFS_SEMA',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
            {
                camada: "DLA",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3ADLA&outputFormat=SHAPE-ZIP",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:DLA',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
            {
                camada: "ARCP",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3AARCP&outputFormat=SHAPE-ZIP",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:ARCP',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
            {
                camada: "LP, LI, LO, LOP",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3AMVW_TITULO_LO_LP_LI_LOP&outputFormat=SHAPE-ZIP",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:MVW_TITULO_LO_LP_LI_LOP',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
        ]
    },
    {
        camada: "Bioma",
        fonte: "SEMA",
        ano: "2022",
        index: 28,
        downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3ABIOMAS_MT&outputFormat=SHAPE-ZIP",
        layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
            attribution: 'SEMAMT',
            layers: 'Sema_Alertas:BIOMAS_MT',
            format: 'image/png',
            transparent: true,
            authkey: SEMAMT_GEOSERVER_AUTHKEY
        } as any),
    },
    {
        camada: "CAR",
        fonte: "SEMA",
        ano: "2022",
        downloadUrl: "https://geo.sema.mt.gov.br/geoserver/web",
        subitens: [
            {
                camada: "CAR ATP",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3AMVW_CAR&outputFormat=SHAPE-ZIP",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:MVW_CAR',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
            {
                camada: "CAR APRT",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3ACAR_APRT&outputFormat=SHAPE-ZIP",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:CAR_APRT',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
            {
                camada: "APRT",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3ASIMLAMGEO_APRT&outputFormat=SHAPE-ZIP",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:SIMLAMGEO_APRT',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
            {
                camada: "SIGEF",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3ASIGEF_INCRA&outputFormat=SHAPE-ZIP",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:SIGEF_INCRA',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
        ]
    },
    {
      camada: "Embargos",
      fonte: "SEMA",
      ano: "2022",
      downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3AMVW_TIT_EMBARGO&outputFormat=SHAPE-ZIP",
      subitens: [
          {
              camada: "SEMA",
              fonte: "SEMA",
              ano: "2022",
              downloadUrl: "",
              layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                  attribution: 'SEMAMT',
                  layers: 'Sema_Alertas:AREAS_EMBARGADAS_SEMA',
                  format: 'image/png',
                  transparent: true,
                  authkey: SEMAMT_GEOSERVER_AUTHKEY
              } as any),
          },
          {
                camada: "IBAMA",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:AREAS_EMBARGADAS_IBAMA',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
          {
                camada: "ICMBio",
                fonte: "SEMA",
                ano: "2022",
                downloadUrl: "",
                layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
                    attribution: 'SEMAMT',
                    layers: 'Sema_Alertas:AREAS_EMBARGADAS_ICMBIO',
                    format: 'image/png',
                    transparent: true,
                    authkey: SEMAMT_GEOSERVER_AUTHKEY
                } as any),
            },
          ]
        },
    {
        camada: "Estradas",
        fonte: "SEMA",
        ano: "2022",
        downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3AAUTORIZACAO_DESMATE_SEMA&outputFormat=SHAPE-ZIP",
        layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
            attribution: 'SEMAMT',
            layers: 'Sema_Alertas:SISTEMA_VIARIO',
            format: 'image/png',
            transparent: true,
            authkey: SEMAMT_GEOSERVER_AUTHKEY
        } as any),
    },
    {
        camada: "Municípios",
        fonte: "SEMA",
        ano: "2022",
        index: 31,
        downloadUrl: "https://geo.sema.mt.gov.br/geoserver/Sema_Alertas/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Sema_Alertas%3ALIM_MUNICIPIOS_MT&outputFormat=SHAPE-ZIP",
        layer: tileLayer.wms(`${SEMAMT_GEOSERVER_URL}/Sema_Alertas/wms`, {
            attribution: 'SEMAMT',
            layers: 'Sema_Alertas:LIM_MUNICIPIOS_MT',
            format: 'image/png',
            transparent: true,
            authkey: SEMAMT_GEOSERVER_AUTHKEY
        } as any),
    },
    {
        camada: "Máscara PRODES",
        fonte: "SEMA",
        ano: "2022",
        downloadUrl: `${GEOSERVER_URL}/${GEOSERVER_WORKSPACE}/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=SEMAMT%3AGEO_PRODES&outputFormat=SHAPE-ZIP`,
        layer: tileLayer.wms(`${GEOSERVER_URL}/${GEOSERVER_WORKSPACE}/wms`, {
            attribution: 'SEMA',
            layers: `${GEOSERVER_WORKSPACE}:GEO_PRODES`,
            format: 'image/png',
            transparent: true
        }),
    },
    {
        camada: "Relevo",
        fonte: "SRTM",
        ano: "2022",
        index: 30,
        downloadUrl: `${GEOSERVER_URL}/SEMAMT/wms?service=WMS&version=1.1.0&request=GetMap&layers=SemaMT%3AMT_srtm&bbox=-61.6357039%2C-18.0437625%2C-50.22456922%2C-7.348650141&width=768&height=719&srs=EPSG%3A4326&styles=&format=image%2Ftiff`,
        layer: tileLayer.wms(`${GEOSERVER_URL}/SemaMT/wms`, {
            attribution: 'SEMA',
            layers: 'SemaMT:MT_srtm',
            format: 'image/png',
            transparent: true
        }),
    },
    {
        camada: "TI",
        fonte: "SEMA",
        ano: "2022",
        downloadUrl: `${GEOSERVER_URL}/${GEOSERVER_WORKSPACE}/wms?service=WMS&version=1.1.0&request=GetMap&layers=SemaMT%3AGEO_TIm&bbox=-61.6357039%2C-18.0437625%2C-50.22456922%2C-7.348650141&width=768&height=719&srs=EPSG%3A4326&styles=&format=image%2Ftiff`,
        layer: tileLayer.wms(`${GEOSERVER_URL}/${GEOSERVER_WORKSPACE}/wms`, {
            attribution: 'SEMA',
            layers: `${GEOSERVER_WORKSPACE}:GEO_TI`,
            format: 'image/png',
            transparent: true
        }),
    },
    {
        camada: "UC",
        fonte: "SEMA",
        ano: "2022",
        downloadUrl: `${GEOSERVER_URL}/${GEOSERVER_WORKSPACE}/wms?service=WMS&version=1.1.0&request=GetMap&layers=SemaMT%3AGEO_UCm&bbox=-61.6357039%2C-18.0437625%2C-50.22456922%2C-7.348650141&width=768&height=719&srs=EPSG%3A4326&styles=&format=image%2Ftiff`,
        layer: tileLayer.wms(`${GEOSERVER_URL}/${GEOSERVER_WORKSPACE}/wms`, {
            attribution: 'SEMA',
            layers: `${GEOSERVER_WORKSPACE}:GEO_UC`,
            format: 'image/png',
            transparent: true
        }),
    },
];
