export const environment = {
  production: false,
  name: '.',
  hmr: false
}; 

export const API_HOST = `http://dev-piaucu.sema.mt.gov.br:9080`;
export const API_HOST_PATH = `alerta-server-novo`;
export const API_BASE_URL = `${API_HOST}/${API_HOST_PATH}/api`;
export const GEOSERVER_URL = "http://dev-piaucu.sema.mt.gov.br:8080/geoserver";
export const SEMAMT_GEOSERVER_URL = "https://geo.sema.mt.gov.br/geoserver";
export const SEMAMT_GEOSERVER_AUTHKEY = "44cb205d-a7ab-482b-a5b1-98d4efd5c3b9";
export const PLANET_API_KEY = 'PLAK57ac7d05297a42e3ad50cb8bfba37c75';
export const GEE_IMAGE_API_BASEURL = 'http://dev-piaucu.sema.mt.gov.br:8001';

//
export const BASE_URL = "http://177.153.58.170";
export const URL_API = `${BASE_URL}:9000/api`;
export const URL_ASSETS = `${BASE_URL}:9000/assets`;
export const URL_GEO_API = `${BASE_URL}:3000`;
