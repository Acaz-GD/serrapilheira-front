// vendobasemap fornece os liks dos mapas

import { tileLayer} from "leaflet";

export class VendorBaseMaps {
  
  static GOOGLE = {
    Maps: {
      url: 'https://mt1.google.com/vt/lyrs=r&x={x}&y={y}&z={z}',
      attribution: 'Google Maps'
    },
    Satellite: {
      url: 'https://mt1.google.com/vt/lyrs=s&hl=en&x={x}&y={y}&z={z}',
      attribution: 'Google Satellite'
    },
    SatelliteHybrid: {
      url: 'https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}',
      attribution: 'Google Satellite Hybrid'
    },
    Terrain: {
      url: 'https://mt1.google.com/vt/lyrs=t&x={x}&y={y}&z={z}',
      attribution: 'Google Terrain'
    },
    Roads: {
      url: 'https://mt1.google.com/vt/lyrs=h&x={x}&y={y}&z={z}',
      attribution: 'Google Roads'
    }
  };

  static OSM = {
    Standard: {
      url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      attribution: 'Open Street Maps'
    },
    Monochrome: {
      url: 'http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
      attribution: 'Open Street Maps'
    }
  };

  static CARTO = {
    Dark: {
      url: 'https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png',
      attribution: 'Open Street Maps'
    }
  };

  static PLANET = {
    Mosaic: {
      url: `https://tiles0.planet.com/basemaps/v1/planet-tiles/planet_medres_normalized_analytic_2022-12_mosaic/gmap/{z}/{x}/{y}.png?api_key=PLAK57ac7d05297a42e3ad50cb8bfba37c75`,
      attribution: 'Planet Mosaic'
    }
  };

  static MapBiomas = {
    Classification: {
      getLayer: () => tileLayer.wms(`http://azure.solved.eco.br:8080/geoserver/solved/wms`, {
        attribution: 'MapBiomas Classification',
        format: 'image/png',
        transparent: true,
        layers: 'solved:mapbiomas_2021',
      } as any),
      getLayer1: () => tileLayer.wms(`https://brasil.mapserver.mapbiomas.org/wms/coverage.map`, {
        attribution: 'MapBiomas Classification',
        format: 'image/png',
        transparent: true,
        layers: 'coverage',
        territory_ids: 1,
        year: 2022,
        class_tree_node_ids: [1, 7, 8, 9, 10, 2, 11, 12, 13, 14, 15, 16, 3, 17, 18, 27, 37, 38, 39, 40, 41, 28, 42, 43, 44, 19, 20, 4, 21, 22, 23, 24, 5, 25, 26, 6]
      } as any)
    }
  };
  static attribution: string | undefined;
}
