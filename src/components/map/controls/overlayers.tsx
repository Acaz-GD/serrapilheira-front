import React from 'react';
import { Button, Paper, Switch, FormControlLabel, FormGroup, Typography, Divider } from '@mui/material';
import LayersIcon from '@mui/icons-material/Layers';
import { styled } from '@mui/material/styles';
import { useModal } from './context/context-modalProvider';

// Estilização do Paper
const StyledPaper = styled(Paper)`
  position: absolute;
  bottom: 4rem;
  left: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 16px;
  z-index: 10; 
`;

// Estilização do Switch para AntSwitch
const AntSwitch = styled(Switch)(({ theme }) => ({
  width: 28,
  height: 16,
  padding: 0,
  margin: 8,
  display: 'flex',
  '&:active': {
    '& .MuiSwitch-thumb': {
      width: 15,
    },
    '& .MuiSwitch-switchBase.Mui-checked': {
      transform: 'translateX(9px)',
    },
  },
  '& .MuiSwitch-switchBase': {
    padding: 2,
    '&.Mui-checked': {
      transform: 'translateX(12px)',
      color: '#fff',
      '& + .MuiSwitch-track': {
        opacity: 1,
        backgroundColor: theme.palette.mode === 'dark' ? '#177ddc' : '#1890ff',
      },
    },
  },
  '& .MuiSwitch-thumb': {
    boxShadow: '0 2px 4px 0 rgb(0 35 11 / 20%)',
    width: 12,
    height: 12,
    borderRadius: 6,
    transition: theme.transitions.create(['width'], {
      duration: 200,
    }),
  },
  '& .MuiSwitch-track': {
    borderRadius: 16 / 2,
    opacity: 1,
    backgroundColor: theme.palette.mode === 'dark' ? '#39393D' : '#E9E9EA',
    boxSizing: 'border-box',
  },
}));

// Posição do botão no canto inferior esquerdo
const StyledButton = styled(Button)`
  bottom: 3rem;
  left: 12px;
  display: flex;
  position: absolute;
  transform: translateY(-50%);
  user-select: none;
  flex-direction: column;
  min-width: 32px;
  min-height: 32px;
  padding: 0;
  background: #002FA7;
  z-index: 10; 
`;

const Overlayers: React.FC = () => {
  const { openModal, closeModal, isModalOpen } = useModal();
  const modalId = 'overlayers';
  const [switchStates, setSwitchStates] = React.useState({
    bioma: false,
    ti: false,
    uc: false,
  });

  const handleClick = () => {
    if (isModalOpen(modalId)) {
      closeModal();
    } else {
      openModal(modalId);
    }
  };

  const handleSwitchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSwitchStates({
      ...switchStates,
      [event.target.name]: event.target.checked,
    });
  };

  return (
    <div>
      <StyledButton variant="contained" onClick={handleClick}>
        <LayersIcon />
      </StyledButton>

      {isModalOpen(modalId) && (
        <StyledPaper elevation={1} sx={{ display: 'flow', zIndex: 20 }}>
          <Typography variant="h6" gutterBottom sx={{ textAlign: 'center', fontSize: '1rem' }}>
            Camadas
          </Typography>
            <Divider sx={{ mb: 2 }} />
          <FormGroup sx={{ zIndex: 20, mr: 4 }}>
            <FormControlLabel 
              control={
                <AntSwitch
                checked={switchStates.bioma}
                onChange={handleSwitchChange}
                name="bioma"
                />
              }
              label="Bioma"
              sx={{ m:0.5 }}
            />
            <FormControlLabel
              control={
                <AntSwitch
                  checked={switchStates.ti}
                  onChange={handleSwitchChange}
                  name="ti"
                />
              }
              label="TI"
              sx={{ m:0.5 }}
            />
            <FormControlLabel
              control={
                <AntSwitch
                  checked={switchStates.uc}
                  onChange={handleSwitchChange}
                  name="uc"
                />
              }
              label="UC"
              sx={{ m:0.5 }}
            />
          </FormGroup>
        </StyledPaper>
      )}
    </div>
  );
};

export default Overlayers;
