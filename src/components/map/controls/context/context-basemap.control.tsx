// BaseMapContext é o controle entre os componentes entre basemap e vendorbaseap

import React, { createContext, useState, useContext, ReactNode } from 'react';

interface BaseMapContextType {
  baseMapUrl: string;
  setBaseMapUrl: (url: string) => void;
}

const BaseMapContext = createContext<BaseMapContextType | undefined>(undefined);

export const BaseMapProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
  const [baseMapUrl, setBaseMapUrl] = useState<string>('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  return (
    <BaseMapContext.Provider value={{ baseMapUrl, setBaseMapUrl }}>
      {children}
    </BaseMapContext.Provider>
  );
};

export const useBaseMap = () => {
  const context = useContext(BaseMapContext);
  if (!context) {
    throw new Error('useBaseMap deve ser usado dentro de um provedor BaseMaps');
  }
  return context;
};
