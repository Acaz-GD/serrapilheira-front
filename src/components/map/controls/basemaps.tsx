import React from 'react';
import { Button, Paper, Radio, RadioGroup, FormControlLabel, FormGroup, Divider, Typography } from '@mui/material';
import MapIcon from '@mui/icons-material/Map';
import { styled } from '@mui/material/styles';
import { useModal } from './context/context-modalProvider';
import { VendorBaseMaps } from '../vendor-map/vendorbasemap';
import { useBaseMap } from './context/context-basemap.control';

// Estilização do Paper
const StyledPaper = styled(Paper)`
  position: fixed;
  bottom: 7rem;
  left: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 16px;
  z-index: 10; 
`;

// Posição do botão no canto inferior esquerdo
const StyledButton = styled(Button)`
  bottom: 6rem;
  left: 12px;
  display: flex;
  position: absolute;
  transform: translateY(-50%);
  user-select: none;
  flex-direction: column;
  min-width: 32px;
  min-height: 32px;
  padding: 0;
  background: #002FA7;
  z-index: 10; 
`;

const isTileLayerConfig = (layer: any): layer is { url: string; attribution: string } => {
  return layer && typeof layer.url === 'string' && typeof layer.attribution === 'string';
};

const Basemaps: React.FC = () => {
  const { setBaseMapUrl } = useBaseMap();
  const { openModal, closeModal, isModalOpen } = useModal();
  const modalId = 'basemaps';

  const handleClick = () => {
    if (isModalOpen(modalId)) {
      closeModal();
    } else {
      openModal(modalId);
    }
  };

  const handleChangeBaseMap = (baseMapKey: string) => {
    const baseMapConfig = baseMapKey.split('.').reduce((acc: any, key: string) => acc ? acc[key] : undefined, VendorBaseMaps);
    if (isTileLayerConfig(baseMapConfig)) {
      setBaseMapUrl(baseMapConfig.url);
    }
  };

  return (
    <div>
      <StyledButton variant="contained" onClick={handleClick}>
        <MapIcon />
      </StyledButton>
      {isModalOpen(modalId) && (
        <StyledPaper elevation={1} sx={{ display: 'flow' }}>
          <Typography variant="h6" gutterBottom sx={{ textAlign: 'center', fontSize: '1rem' }}>
            Mapa base
          </Typography>
          <Divider sx={{ mb: 2 }} />
          <FormGroup>
            <RadioGroup onChange={(e) => handleChangeBaseMap(e.target.value)}>
              <FormControlLabel value="GOOGLE.Maps" control={<Radio />} label="Google Maps" />
              <FormControlLabel value="GOOGLE.Satellite" control={<Radio />} label="Google Satellite" />
              <FormControlLabel value="GOOGLE.SatelliteHybrid" control={<Radio />} label="Google Satellite Hybrid" />
              <FormControlLabel value="GOOGLE.Terrain" control={<Radio />} label="Google Terrain" />
              <FormControlLabel value="GOOGLE.Roads" control={<Radio />} label="Google Roads" />
              <FormControlLabel value="OSM.Standard" control={<Radio />} label="OSM Standard" />
              {/* <FormControlLabel value="OSM.Monochrome" control={<Radio />} label="OSM Monochrome" /> */}
              <FormControlLabel value="CARTO.Dark" control={<Radio />} label="CARTO Dark" />
              {/* <FormControlLabel value="PLANET.Mosaic" control={<Radio />} label="Planet Mosaic" /> */}
            </RadioGroup>
          </FormGroup>
        </StyledPaper>
      )}
    </div>
  );
};

export default Basemaps;
