import { useState, useEffect } from 'react';
import { Button, Paper, Typography, Box } from '@mui/material';
import QueryStatsIcon from '@mui/icons-material/QueryStats';
import { styled } from '@mui/material/styles';

// Estilização do Paper
const StyledPaper = styled(Paper)`
  position: absolute;
  bottom: 1rem;
  left: 50px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 14px;
  width: 220px;
  z-index: 10; 
`;

// Posição do botão no canto inferior esquerdo
const StyledButton = styled(Button)`
  bottom: 0rem;
  left: 12px;
  display: flex;
  position: absolute;
  transform: translateY(-50%);
  user-select: none;
  flex-direction: column;
  min-width: 32px;
  min-height: 32px;
  padding: 0;
  background: #002FA7;
  z-index: 10; 
`;

// Barra de gradiente baixa e alta prioridade
const GradientBar = styled(Box)`
  width: 100%;
  height: 20px;
  background: linear-gradient(
    to right,
    #fcffe5 10%,
    #efff6e 31%,
    #f2b13a 53%,
    #ff3d45 78%,
    #ff1f9e 100%
  );
  margin: 4px 0;
`;

const PriorityAreas: React.FC = () => {
  const [open, setOpen] = useState(true); // Inicialize como true para manter aberto

  const handleClick = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  
  useEffect(() => { // # Atalho de controle = ctrl+i
    const handleKeyDown = (event: KeyboardEvent) => {
      if (event.ctrlKey && event.key === 'i') {
        event.preventDefault();
        setOpen((prevOpen) => !prevOpen);
      }
    };

    window.addEventListener('keydown', handleKeyDown);
    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, []);

  return (
    <div>
      <StyledButton variant="contained" onClick={handleClick}>
        <QueryStatsIcon />
      </StyledButton>
      {open && (
        <StyledPaper elevation={1}>
          <Typography
            variant="body1"
            gutterBottom
            sx={{ fontSize: '0.8rem', fontWeight: '600', color: '#505050' }}
          >
            Indicador de Áreas Prioritarias
          </Typography>
          <GradientBar />
          <Box display="flex" justifyContent="space-between" width="100%">
            <Typography
              variant="body2"
              sx={{ fontSize: '0.6rem', letterSpacing: '1px', color: '#505050' }}
            >
              Baixa Prioridade
            </Typography>
            <Typography
              variant="body2"
              sx={{ fontSize: '0.6rem', letterSpacing: '1px', color: '#505050' }}
            >
              Alta Prioridade
            </Typography>
          </Box>
        </StyledPaper>
      )}
    </div>
  );
};

export default PriorityAreas;
