import React, { useState } from 'react';
import { MapContainer, TileLayer } from 'react-leaflet';
import Filter from '../filter/filter';
import DrawerPainel from '../drawer/drawerPainel';
import Overlayers from './controls/overlayers';
import Basemaps from './controls/basemaps';
import { BaseMapProvider, useBaseMap } from './controls/context/context-basemap.control';
import Header from '../header/header';
import PriorityAreas from './controls/priorityAreas';
import { ModalProvider } from './controls/context/context-modalProvider';
import PresentationPanel from './presentationPanel';



const MapContent: React.FC = () => {
  const { baseMapUrl } = useBaseMap();

  return (
    <MapContainer
      center={[-27.640705770126306, -48.67549352944665]}
      zoom={10}
      style={{ position: 'absolute', width: '100%', height: '100%', top: '0px', overflow: 'hidden', zIndex: 0 }}
    >
      <TileLayer url={baseMapUrl} />
    </MapContainer>
  );
};

const LeafletMap: React.FC = () => {
  const [open, setOpen] = useState(true);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <BaseMapProvider>
      <PresentationPanel open={open} onClose={handleClose} />

      {/* Topo da pagina */}
      <Header />

      {/* Conteiner de Filtros */}
      <Filter />

      {/* Painel de Estatisticas */}
      <DrawerPainel />

      {/* Controles de Mapa */}
      <ModalProvider>
        <Overlayers />
        <Basemaps />
        <PriorityAreas />
      </ModalProvider>

      {/* Mapa */}
      <MapContent />
    </BaseMapProvider>
  );
};

export default LeafletMap;
