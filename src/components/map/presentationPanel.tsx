// PresentationPanel.tsx
import React from 'react';
import { Dialog, DialogContent, DialogTitle, Button, IconButton, Typography, Card, CardContent, CardActions, Box, List, ListItem, ListItemIcon, ListItemText, Grid } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import PersonIcon from '@mui/icons-material/Person';
import DashboardIcon from '@mui/icons-material/Dashboard';
import StorageIcon from '@mui/icons-material/Storage';
import MapIcon from '@mui/icons-material/Map';
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';
import { ReactComponent as Logo } from '../assets-components/logoSerrailheira.svg';

interface PresentationPanelProps {
  open: boolean;
  onClose: () => void;
}

const PresentationPanel: React.FC<PresentationPanelProps> = ({ open, onClose }) => {
  return (
    <Dialog open={open}
      onClose={onClose}
      fullWidth
      maxWidth="sm"
      PaperProps={{
        sx: { backgroundColor: '#FFE8CF' }
      }}>
      <DialogTitle sx={{ m: 0, p: 2 }}>
        <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <Logo style={{ margin: 16, height: 40 }} />
        </Box>
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>

      <Typography variant="h6" component="div" sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        Bem-vindo ao nosso projeto!
      </Typography>

      <DialogContent dividers sx={{ borderTop: 0 }}>
        <Typography gutterBottom sx={{ mt: 0, mb: 0, ml: 8, mr: 8, textAlign: 'center' }}>
         Olá usuário! Aqui você pode explorar todas as funcionalidades e encontrará informações detalhadas e ferramentas úteis para explorar as áreas prioritárias.
        </Typography>

        <Grid container spacing={2} sx={{ mt: 2 }}>
          {/* IMPLEMENTADOR */}
          <Grid item xs={12} sm={6}>
            <Card sx={{ borderRadius: 4 , m: 1, width: '16rem'}}>
              <CardContent sx={{ textAlign: 'center' }}>
                <PersonIcon sx={{ fontSize: 50, }} />
                <Typography variant="h6">
                  Implementadores
                </Typography>
                <List>
                  <ListItem>
                    <ListItemIcon>
                      <DashboardIcon sx={{ color: '#217AB8' }} />
                    </ListItemIcon>
                    <ListItemText primary="Acesso Completo a Plataforma" />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <StorageIcon sx={{ color: '#217AB8' }} />
                    </ListItemIcon>
                    <ListItemText primary="Uso de Dados Científicos" />
                  </ListItem>
                </List>
              </CardContent>
              <CardActions sx={{ justifyContent: 'center' }}>
                <Button onClick={onClose} color="primary" variant="contained"
                sx={{ background: '#4F4F4F' , mb: 1, width: '10rem', borderRadius: '8px'}}>
                  Explorar
                </Button>
              </CardActions>
            </Card>
          </Grid>

          {/* PUBLICO */}
          <Grid item xs={12} sm={6}>
            <Card sx={{ borderRadius: 4 , m: 1, width: '16rem'}}>
              <CardContent sx={{ textAlign: 'center' }}>
                <PersonIcon sx={{ fontSize: 50, }} />
                <Typography variant="h6">
                  Público
                </Typography>
                <List>
                  <ListItem>
                    <ListItemIcon>
                      <MapIcon sx={{ color: '#217AB8' }} />
                    </ListItemIcon>
                    <ListItemText primary="Acesso Livre para Consultas" />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CloudDownloadIcon sx={{ color: '#217AB8' }} />
                    </ListItemIcon>
                    <ListItemText primary="Download de arquivos" />
                  </ListItem>
                </List>
              </CardContent>
              <CardActions sx={{ justifyContent: 'center' }}>
                <Button onClick={onClose} color="primary" variant="contained" 
                  sx={{ background: '#4F4F4F' , mb: 1, width: '10rem', borderRadius: '8px'}}>
                  Explorar
                </Button>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </DialogContent>
    
    </Dialog>
  );
};

export default PresentationPanel;
