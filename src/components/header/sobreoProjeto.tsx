import React, { useState } from 'react';
import { Modal, Box, Typography } from '@mui/material';
// import InfoIcon from '@mui/icons-material/Info';
import { ReactComponent as Logo } from './logoSerrailheira.svg';
import { ReactComponent as InfoHeader } from '../assets-components/infoHeader.svg'

const AboutModal: React.FC = () => {
  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Box
        onClick={handleOpen}
        sx={{
          display: 'flex',
          alignItems: 'center',
          cursor: 'pointer',
          '&:hover': {
            color: 'orange',
          }
        }}
      >
        <Typography variant="button" component="span" 
        sx={{
          margin: '5px 5px 0px 0px',
          fontFamily: 'Arial, Helvetica, sans-serif',
          fontSize: '10px',
          color: '#282633',
          fontWeight: 700,
          cursor: 'pointer',
          '&:hover': {
            color: 'orange',
          }
        }}>
          Sobre o Projeto
        </Typography>
        {/* <InfoIcon sx={{ ml: 1 }} /> */}
        <InfoHeader />
      </Box>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-title"
        aria-describedby="modal-description"
      >
        <Box 
          sx={{
            position: 'absolute' as 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 400,
            bgcolor: 'background.paper',
            border: '2px solid #000',
            boxShadow: 24,
            p: 4,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Logo style={{ width: '10rem' }} />
          <Typography id="modal-title" variant="h6" component="h2" sx={{ mt: 2 }}>
            Pagina de Apresentação
          </Typography>
          <Typography id="modal-description" sx={{ mt: 2 }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent porttitor aliquam vehicula. 
            Curabitur nec finibus mauris. Praesent id diam ultrices, pellentesque dui vitae, euismod urna. 
            Nam nec ultrices eros, id feugiat justo. Interdum et malesuada fames ac ante ipsum primis in faucibus. 
            Nam vitae dui a quam cursus iaculis in id tortor. Praesent sollicitudin nunc tellus, ut hendrerit 
            lacus efficitur vulputate. Phasellus egestas luctus mi volutpat tempor. Praesent cursus metus ac massa
            iaculis, mollis fringilla dui pulvinar. Nam nec lacus iaculis, scelerisque nisi et, placerat mi. Sed 
            fermentum posuere condimentum. In tincidunt, arcu quis commodo aliquam, lorem purus porta lacus, ac 
            iaculis sapien erat quis nulla.
          </Typography>
        </Box>
      </Modal>
    </div>
  );
};

export default AboutModal;
