import React from 'react';
import { Container, Divider } from '@mui/material';
import { ReactComponent as Logo } from './logoSerrailheira.svg';
import SobreoProjeto from './sobreoProjeto';

const Header: React.FC = () => {
  return (
    <Container
      sx={{
        position: 'absolute',
        maxWidth: '100% !important',
        height: '3rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottom: '1px solid #D3D3D3',
        backgroundColor: '#FFF',
        zIndex: '10',
      }}
    >
      {/* Ícone SVG Brand */}
      <Logo style={{ width: '10rem' }} />

      <Divider />

      <SobreoProjeto />
    </Container>
  );
};

export default Header;
