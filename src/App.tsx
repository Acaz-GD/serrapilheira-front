import React from 'react';
// import '../styles/reset.css';
// import '../styles/globals.css';
import Home from './components/map/home';
import Container from '@mui/material/Container';

const HomePage: React.FC = () => {
  return (
    <Container>
      {/* Pagina Principal / Mapa */}
        <Home />
    </Container>
   
  );
};

export default HomePage;

// [-27.640705770126306, -48.67549352944665];
